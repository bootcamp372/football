"use strict"

let teams = [
    {
        code: "DAL",
        name: "Dallas Cowboys",
        plays: "Arlington, TX"
    },
    {
        code: "DEN",
        name: "Denver Broncos",
        plays: "Denver, CO"
    },
    {
        code: "HOU",
        name: "Houston Texans",
        plays: "Houston, TX"
    },
    {
        code: "KAN",
        name: "Kansas City Chiefs",
        plays: "Kansas City, MO"
    }
];

window.onload = function () {
    const displayBtn = document.getElementById("displayBtn");
    const clearBtn = document.getElementById("clearBtn");
    const addTeamForm = document.getElementById("addTeamForm");
    const footballList = document.getElementById("footballList");
    initStatesDropdown(teams);
    footballList.onchange = displayImg;
    displayBtn.onclick = displayInfo;
    addTeamForm.onsubmit = addTeam;
    clearBtn.onclick = clearValues;
};

function initStatesDropdown(teams) {

    const footballList = document.getElementById("footballList");
    let teamsLength = teams.length;

    let selectTeamOption = new Option("Select a team", "");
    footballList.appendChild(selectTeamOption);

    for (let i = 0; i < teamsLength; i++) {
        let teamOption = new Option(teams[i].name, teams[i].code);
        footballList.appendChild(teamOption);
    }
}

function displayInfo() {
    const footballList = document.getElementById("footballList");
    const selectedOptions = footballList.selectedOptions;
    let matchingOptionCodes = [];
    let numTeams = teams.length;

    footballInfo.innerHTML="";

    if (footballList.selectedIndex === 0) {
        footballInfo.innerHTML = "";

    }

    //for each selected option, look up info from teams array and display football info
    for (let j = 0; j < selectedOptions.length; j++) {
        matchingOptionCodes.push(selectedOptions[j].value);
    }

    for (let k = 0; k < numTeams; k++) {
        if (matchingOptionCodes.includes(teams[k].code)) {
            console.log(teams[k]);

            let message = `You selected the ${teams[k].name} (${teams[k].code}) who play in ${teams[k].plays}.`;
            let para = document.createElement("p");
            para.id = teams[k].code + "-para";
            para.innerHTML = message;
            footballInfo.append(para);
        }
    }
    return false;
}

function addTeam() {
    const footballList = document.getElementById("footballList");
    const teamName = document.getElementById("teamName").value;
    const teamCode = document.getElementById("teamCode").value;
    const teamPlays = document.getElementById("teamPlays").value;

    let teamOption = new Option(teamName, teamCode);
    footballList.appendChild(teamOption);
    teams.push({
        code: teamCode,
        name: teamName,
        plays: teamPlays
    });
    return false;
}

function clearValues() {
    document.getElementById("teamName").value = "";
    document.getElementById("teamCode").value = "";
    document.getElementById("teamPlays").value = "";

}

function displayImg() {
    const footballList = document.getElementById("footballList");
    const imgDiv = document.getElementById("imgDiv");
    const selectedOptions = footballList.selectedOptions;
    let matchingOptionCodes = [];
    let numTeams = teams.length;

    imgDiv.innerHTML = "";

    //for each selected option, look up info from teams array and display football info
    for (let j = 0; j < selectedOptions.length; j++) {
        matchingOptionCodes.push(selectedOptions[j].value);
    }

    for (let k = 0; k < numTeams; k++) {

        if (matchingOptionCodes.includes(teams[k].code)) {
            let image = document.createElement("IMG");
            image.src = '/images/' + teams[k].code + '.png';
            image.id = teams[k].code + "-img";
            imgDiv.append(image);
        }
    }

}